import time
import os
import re
import sys
import json
import subprocess
import requests
import logging
import tempfile
from datetime import datetime
from villas.node.node import Node as VILLASnode


# This could be moved to the DPsim Python code later
def get_shmem_interface_config():
    return {
            'type': 'shmem',
            'in': {
                'name': '/dpsim1-villas',
                'hooks': [
                    {'type': 'stats'}
                ],
                'signals': {
                    'count': 30,
                    'type': 'float'
                }
            },
            'out': {
                'name': '/villas-dpsim1'
            }
        }

def get_villas_config(name, node_type):
    if node_type == "ws":
        return {
            'http': {
                'port': 80
            },
            'hugepages': 0,
            'nodes': {
                'relay': {
                    'type': 'websocket',
                    'destinations': [
                        f'https://villas.k8s.eonerc.rwth-aachen.de/ws/relay/{name}'
                    ]
                },
                'dpsim': get_shmem_interface_config(),
            },
            'paths': [
                {
                    'in': 'dpsim',
                    'out': 'relay',

                    'hooks': [
                        {
                            'type': 'limit_rate',
                            'rate': 20
                        }
                    ]
                },
                {
                    'in': 'relay',
                    'out': 'dpsim',

                    'hooks': [
                        {
                            'type': 'limit_rate',
                            'rate': 20
                        }
                    ]
                }

            ],
            'logging': {
                'level': 'debug'
            }
        }
    elif node_type == "mqtt":
        return {
            'http': {
                'port': 80
            },
            'hugepages': 0,
            'nodes': {
                'broker': {
                    'type': 'mqtt',
                    'format': 'json',
                    'host': 'test.mosquitto.org',
                    'out': {
                        'publish': '/dpsim-powerflow'
                    }
                },
                'dpsim': get_shmem_interface_config(),
            },
            'paths': [
                {
                    'in': 'dpsim',
                    'out': 'broker',

                    'hooks': [
                        {
                            'type': 'limit_rate',
                            'rate': 50
                        }
                    ]
                }
            ],
            'logging': {
                'level': 'debug'
            }
        }
    else:
        if node_type is None:
            logging.error('Specification of node type required for villas. Exiting program.')
        else:
            logging.error('Unsupported node type for villas. Exiting program.')
        sys.exit()

def get_villas_payload():
    env = os.environ.get('VILLAS_PAYLOAD')
    fn = os.environ.get('VILLAS_PAYLOAD_FILE')

    payload = {}

    if env:
        json_payload = json.loads(env)

        payload.update(json_payload)

    if fn:
        with open(fn) as f:
            file_payload = json.load(f)

            payload.update(file_payload)

    return payload

def download_models(models, dir):

    type = models.get('type', 'url')
    if type not in ['url', 'url-list']:
        raise RuntimeError('Invalid model type')

    urls = []

    urls = models.get('url')
    if not urls:
        raise RuntimeError('Missing model url')

    if type == 'url':
        urls = [ urls ]
    elif type == 'url-list':
        pass

    token = models.get('token')
    headers = {}
    if token:
        headers['Authorization'] = f'Bearer {token}'

    for url in urls:
        logging.info('Downloading model: %s', url)

        r = requests.get(url,
            headers=headers)
        r.raise_for_status()

        try:
            d = r.headers['content-disposition']
            filename = re.findall("filename=(.+)", d)[0]
        except KeyError:
            parts = url.split("/")
            filename = parts[len(parts)-1]

        # TODO: delete, this is temporary
        if filename == '784':
            filename = 'Rootnet_FULL_NE_06J16h_TP.xml'
        elif filename == '785':
            filename = 'Rootnet_FULL_NE_06J16h_SV.xml'
        elif filename == '786':
            filename = 'Rootnet_FULL_NE_06J16h_EQ.xml'
        elif filename == '787':
            filename = 'Rootnet_FULL_NE_06J16h_DI.xml'

        with open(os.path.join(dir, filename), 'wb') as f:
            f.write(r.content)

    # print contents of dir with downloaded models
    arr = os.listdir(dir)
    logging.info(arr)

def upload_results(results, dir, resfiles):

    if results.get('type') != 'url':
        raise RuntimeError('Invalid result type')

    url = results.get('url')
    token = results.get('token')

    headers = {}
    if token:
        headers['Authorization'] = f'Bearer {token}'

    for subdir, dirs, files in os.walk(dir):
        for file in files:
            fn = os.path.join(subdir, file)
            if file != "" and resfiles and not fn.endswith(tuple(resfiles)):
                logging.info('Not uploading: %s', file)
                continue

            logging.info('Uploading %s', fn)

            with open(fn, 'rb') as f:

                files = {
                    'file': f
                }

                r = requests.post(url,
                    files=files,
                    headers=headers
                )
                r.raise_for_status()

def dpsim_start(params, when=None, via='command-line-opts'):
    if when is not None:
        start_at = datetime.fromtimestamp(when)
        params['start-at'] = start_at.isoformat()

    cmd = [
        params.get('executable')
    ]

    if via == 'command-line-opts':

        param_names = [
            'start-synch',
            'steady-init',
            'blocking',
            'timestep',
            'duration',
            'system-freq',
            'scenario',
            'log-level',
            'start-at',
            'start-in',
            'solver-domain',
            'solver-type',
            'name'
        ]

        for param_name in param_names:
            if param_name in params:
                value = params[param_name]

                if type(value) is bool:
                    if value is True:
                        cmd += [f'--{param_name}']
                else:
                    cmd += [f'--{param_name}', str(value)]

        options = params.get('options', {})
        for key, value in options.items():
            cmd += ['--option', f'{key}={value}']

        cmd += params.get('files', [])

    elif via == 'json':
        with open('parameters.json', 'w+') as f:
            json.dump(params, f)

        cmd += ['--params', 'parameters.json']

    logging.info('DPsim command: ' + str(cmd))
    return subprocess.Popen(cmd)

def villas_start(config):
    node = VILLASnode(
        log_filename='villas-node.log',
        config=config
    )

    logging.info('Starting VILLASnode gateway')
    node.start() # VILLASnode starts running in the background from here..

    logging.info('VILLASnode running?: %s', node.is_running())

    logging.info(node.config_file.name)

    with open(node.config_file.name) as f:
        sys.stdout.write(f.read())
        sys.stdout.flush()

    tries = 20
    while tries:
        try:
            logging.info(node.nodes)
            dump_node_log(node)
            break
        except Exception as e:
            logging.warning('Trying to reach VILLASnode. Try %d', tries)
            logging.warning(e)
            time.sleep(1)
            tries -= 1
    if not tries:
        dump_node_log(node)
        logging.error('Giving up')
        sys.exit(-1)

    return node

def villas_dump(node):
    # Some infos from the running VILLASnode instance queried via its REST API
    try:
        # logging.info('VILLASnode status: ', node.status)
        logging.info('VILLASnode nodes: %s', node.nodes)
        logging.info('VILLASnode paths: %s', node.paths)
        logging.info('VILLASnode config: %s', node.active_config)
        # logging.info('VILLASnode version: %s', node.get_version())
    except:
        dump_node_log(node)

def dump_node_log(node):
    with open(node.log_filename) as f:
        sys.stdout.write(f.read())
        sys.stdout.flush()

def main():
    # Match DPsim spdlog loglevels
    logging.addLevelName(logging.CRITICAL, 'critial')
    logging.addLevelName(logging.ERROR, 'error')
    logging.addLevelName(logging.WARNING, 'warn')
    logging.addLevelName(logging.INFO, 'info')
    logging.addLevelName(logging.DEBUG, 'debug')

    logging.basicConfig(level=logging.DEBUG,
        format='[%(asctime)s.%(msecs)d %(name)s %(levelname)s] %(message)s',
        datefmt='%H:%M:%S')

    payload = get_villas_payload()
    params = payload.get('parameters', {})

    cwd = tempfile.mkdtemp()
    logging.info('Working directory: %s', cwd)
    os.chdir(cwd)

    models = payload.get('model')
    if models:
        download_models(models, cwd)
    else:
        logging.warning('No model files to download')

    villas = params.get('villas')

    if villas and villas.get('enabled', True):

        logging.info("VILLASnode enabled.")

        villas_node_type = villas.get('type')
        villas_config = get_villas_config('slew', villas_node_type)

        #villas_config_params = villas.get('config', {})
        #villas_config.update(villas_config_params)

        node = villas_start(
            config=villas_config
        )

        villas_dump(node)
    else:
        node = None

    logging.info('Starting DPsim simulation')
    sim = dpsim_start(
        params=params,
        when=payload.get('when'),
        via='json'
    )

    sim.wait()

    if node:
        node.stop()

    results = payload.get('results')

    # if set, resultfiles allows to upload only specific files
    resfiles = params.get('resultfiles', [])

    if results:
        results_dir = os.path.join(cwd, 'logs')
        logging.info('Uploading results of %s', results_dir)
        upload_results(results, results_dir, resfiles)
    else:
        logging.warning('Results are not uploaded')

if __name__ == '__main__':
    main()

# Development

## Format of VILLAS start message payload

https://villas.fein-aachen.org/doc/controller-protocol.html


## Usage

Using built image from: https://hub.docker.com/repository/docker/dpsimrwth/slew-villas

### Run locally

```bash
cd Image
docker run --privileged -e VILLAS_PAYLOAD_FILE=/payload.json -v $(pwd)/payloads/<json-file-name>.json:/payload.json dpsimrwth/slew-villas
```

## Development

Building image from Dockerfile.

```bash
cd Image
```

### Build SLEW image

```bash
docker build --file Dockerfile.slew -t dpsimrwth/slew .
docker login
docker push dpsimrwth/slew:latest
```
### Build SLEW-VILLAS image

```bash
docker build --file Dockerfile.slew-villas -t dpsimrwth/slew-villas .
docker login
docker push dpsimrwth/slew-villas:latest
```

### Run locally

```bash
docker run --privileged -e VILLAS_PAYLOAD_FILE=/payload.json -v $(pwd)/payloads/<json-file-name>.json:/payload.json dpsimrwth/slew-villas
```


